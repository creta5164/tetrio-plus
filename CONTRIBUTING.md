TETR.IO PLUS uses the MIT license, and to prevent licensing issues all contributions must be licensed under the same.

Note: Release schedule is not well defined, typically only releasing for large game breaking related fixes or new features, so any small contribution you make could take some time to make it into a release.
